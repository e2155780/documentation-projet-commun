---
title: "Exercices"
weight: 5
date: 2022-01-11T14:19:27Z
draft: false
---

### Exercice 1: fast-forward
Placez dans votre dépôt deux fichiers, *doc1.txt* et *doc2.txt*, ayant le contenu suivant:  

--- Fichier doc1.txt ---   
Ligne 1  
Ligne 2  
Ligne 3  
Ligne 4  
Ligne 5  
Ligne 6  
Ligne 7  
Ligne 8  
Ligne 9  
Ligne 10  

Faites maintenant les manipulations suivantes, en vérifiant à chaque étape si vous avez bien compris ce qui s'est passé.  

|     Équipier 1                         |     Équipier 2                          | Remarques                                         |
|----------------------------------------|-----------------------------------------|---------------------------------------------------|
|     Modifier la ligne 1 de doc1.txt    |                                         |                                                   |
|     add, commit, push                  |                                         |                                                   |
|                                        |     fetch                               |                                                   |
|     status                             |     status                              |     Comparer le résultat                          |
|     log                                |     log                                 |     Comparer le résultat                          |
|                                        |   Modifier la ligne 1 de doc2.txt       |                                                   |
|                                        |     add, commit                         |                                                   |
|                                        |     status                              | Que veut dire « your branch and 'origin/main' have   diverged » ? |
|                                        |   pull                                  |                                                   |
|                                        |     status                              |     Comment expliquer les 2 nouveau commits ?     |
|                                        | push                                    |                                                   |
|     fetch                                    |                                         |                                                   |
|     status                             |                                         |     Que veut dire « your branch …, and can be   fast-forwarded. » ?      |
|     pull                                     |                                         |                                                   |


### Exercice 2: merge automatique

|     Équipier 1                         |     Équipier 2                          |     Remarques                                     |
|----------------------------------------|-----------------------------------------|---------------------------------------------------|
|     Modifier la ligne 1 de doc1.txt    |                                         |                                                   |
|     add, commit                        |                                         |                                                   |
|     push                               |                                         |                                                   |
|                                        |     Modifier la ligne 10 de doc1.txt    |                                                   |
|                                        |     add, commit                         |                                                   |
|                                        |     push                                |     Pourquoi le push ne fonctionne pas ?          |
|                                        |     pull                                |     Que veut dire « Auto-merging doc1.txt » ?     |
|                                        |     log                                 |     Comment expliquer les 2 nouveaux commits ?    |
|                                        |     push                                |                                                   |
|                                        |     log                                 |                                                   |
|     pull                               |                                         |                                                   |
