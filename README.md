# Exercice sur les branches

## Suivez les indications suivantes:  

1. A: Faites un fork de ce dépôt dans votre espace de travail.  
2. AB: Clonez ce dépôt  
3. B: Créez une branche **Dev** à partir de la branche **main**  
4. B: Positionnez-vous sur la branche **Dev**
5. B: Le dossier **Modifications** contient les modifications successives que vous apporterez au projet. Copiez tous le contenu du dossier **Modifications/Modifs 1** à la racine de votre projet.
6. B: Faites un commit de vos modifications avec le message **Modifications 1**  
7. B: Vous devriez avoir 11 fichiers modifiés dans ce commit.
8. B: Poussez votre branche **Dev** sur le dépôt distant.
9. A: Récupérez la branche **Dev** du dépôt distant.
10. A: Copiez maintenant le contenu du dossier **Modifications/Modifs 2** à la racine de votre projet.   
11. A: Faites un commit de vos modifications avec le message **Modifications 2**. Vous devriez avoir 6 fichiers modifiés dans ce commit. 
12. A: Poussez votre commit sur le dépôt distant.  
13. B: Récupérez le contenu de la branche **Dev** du dépôt distant.

Vous avez maintenant un dépôt modifié. Ce dépôt contient en fait une partie du site du cours, écrit en markdown. Vous allez prendre ce site en markdown et le pousser sur le Wiki de votre exercice sur Gitlab. Le wiki de Gitlab fonctionne en markdown lui aussi. De plus, le wiki de Gitlab est organisé en dépôt Git pour en faciliter la gestion des modifications et de l'historique.  

## Édition d'un wiki Gitlab

1. A: Clonez le dépôt du wiki de votre exercice.  
2. A: Copiez les fichiers de votre premier dépôt (branche **Dev**) dans ce nouveau dépôt.
3. A: Poussez le tout dans le dépot du wiki.
4. A: Vous devriez voir un début de table de matières sur votre wiki.

Documentation sur le markdown du wiki de Gitlab: https://docs.gitlab.com/ee/user/markdown.html 
